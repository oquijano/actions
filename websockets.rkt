#lang racket/base

(require crypto
	 racket/fixnum
	 racket/match
	 racket/port
	 web-server/http/request-structs
	 net/rfc6455/http
	 net/rfc6455/service-mapper
	 net/rfc6455
	 net/rfc6455/conn-api
	 net/url
	 json
	 threading
	 uuid
	 (prefix-in receiver: "receiver_connections")
	 )

(define receiver-key (make-parameter (void)))

(define sender-state (make-hash))

(define (crypto-generate-random-string len)
  (with-output-to-string
    (lambda ()
      (for ([b (in-bytes (crypto-random-bytes (/ len 2)))])
        (when (fx< b 16)
          (display "0"))
        (display (number->string b 16))))))


(define (ws-request? req)
	 ;; Returns true if the request corresponds to a websocket
  (define headers (request-headers/raw req))
  
  (define (get-header k)
    (define h (headers-assq* k headers))
    (and h (header-value h)))

  (define (header-equal? k expected)
    (define actual (get-header k))
    (and actual (string-ci=? (bytes->string/latin-1 actual) expected)))

	 
  (define websocket-key (get-header #"Sec-WebSocket-Key"))
  (and websocket-key
       (header-equal? #"Sec-WebSocket-Version" "13")
       (member "websocket" (tokenize-header-value (get-header #"Upgrade")))
       (member "upgrade" (tokenize-header-value (get-header #"Connection")))))


(define (get-id req)
  ;; Extracts the id from a request
  
  ;; Extacts the path from a request. The id is the last element in
  ;; the path.
  (path/param-path (list (url-path (request-uri req)))))

(define (send-error c message)
  (~> (make-hash `((error . ,message)))
      (jsexpr->string)
      (ws-send! c _)))

(define (sender-add-action sender-name uuid action-name parameters)
  (unless (hash-has-key? sender-state sender-name)
    (hash-set! sender-state sender-name (make-hash)))
  
  )

(define (handle-receiver-connection uuid)
  (match (hash-ref receiver:connections uuid)
    [(hash ('name name) ('actions actions) (connection ws-conn))
     (for ([(action-name action-hash) actions ])
       (match action-hash
	 [(hash ('senders senders) ('parameters parameters))
	  (for ([sender senders])
	    (hash-set! sender-state sender
		       (make-hash)))
	  
	  ]
	 )
       )
     ])
  )

(define (handle-receiver-messages)
  (let loop ()
    (match (thread-receive)
      [(list uuid 1)
       (handle-receiver-connection uuid)]
      [(list uuid 2)
       (handle-receiver-disconnection uuid)])    
    (loop)))




(ws-service-mapper
 [(format "/~a" (receiver-key))
  (lambda (c)
    (receiver:handle-connection c (current-thread)))]
 )
